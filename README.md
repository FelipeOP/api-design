# Topic: Catalogue - Library (Books, Authors, Categories)

## Description of Entities and Operations

### Entities

#### Book

- id (integer): Unique identifier for the book.
- title (string): Title of the book.
- authorId (integer): Unique identifier for the author.
- categoryId (integer): Unique identifier for the category.
- publicationYear (integer): Year the book was published.
- description (string): Brief description of the book.

#### Author

- id (integer): Unique identifier for the author.
- name (string): Name of the author.
- bio (string): Biography of the author.

#### Category

- id (integer): Unique identifier for the category.
- name (string): Name of the category.

### Operations

#### Book Operations

- Create a new book.
- Retrieve a list of books with pagination and filters.
- Retrieve a single book by its ID.
- Update a book by its ID.
- Delete a book by its ID.
  
#### Author Operations

- Create a new author.
- Retrieve a list of authors with pagination and filters.
- Retrieve a single author by their ID.
- Update an author by their ID.
- Delete an author by their ID.

#### Category Operations

- Create a new category.
- Retrieve a list of categories.
- Retrieve a single category by its ID.
- Update a category by its ID.
- Delete a category by its ID.

----------

## REST API Design

### Functional and Non-Functional Requirements

#### Functional Requirements

- CRUD operations for books, authors, and categories.
- Filtering and searching for books by title, author, and category.
- Pagination for listing books and authors.

#### Non-Functional Requirements

- The API should be secure, using token-based authentication.
- Responses should be in JSON format.
- Meaningful HTTP status codes should be used.
- API should adhere to REST principles and the Richardson Maturity Model.
- Errors should be clearly defined and communicated.
- Caching should be implemented for GET requests.

----------

## REST API Description using APIDOC

### Book Endpoints

```javascript
/**
 * @api {post} /books Create a new book
 * @apiName CreateBook
 * @apiGroup Book
 *
 * @apiBody {String} title Title of the book.
 * @apiBody {Number} authorId Author's unique ID.
 * @apiBody {Number} categoryId Category's unique ID.
 * @apiBody {Number} publicationYear Year of publication.
 * @apiBody {String} description Description of the book.
 *
 * @apiSuccess {Number} id The new book's ID.
 * @apiSuccess {String} title Title of the book.
 * @apiSuccess {Number} authorId Author's unique ID.
 * @apiSuccess {Number} categoryId Category's unique ID.
 * @apiSuccess {Number} publicationYear Year of publication.
 * @apiSuccess {String} description Description of the book.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "title": "Book Title",
 *       "authorId": 1,
 *       "categoryId": 1,
 *       "publicationYear": 2020,
 *       "description": "A brief description of the book."
 *     }
 *
 * @apiError BadRequest The request body is invalid.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Invalid request body"
 *     }
 */

/**
 * @api {get} /books Retrieve a list of books
 * @apiName GetBooks
 * @apiGroup Book
 *
 * @apiParam {Number} [page=1] Page number for pagination.
 * @apiParam {Number} [limit=10] Number of items per page.
 * @apiParam {String} [title] Filter books by title.
 * @apiParam {Number} [authorId] Filter books by author ID.
 * @apiParam {Number} [categoryId] Filter books by category ID.
 *
 * @apiSuccess {Object[]} books List of books.
 * @apiSuccess {Number} books.id Book ID.
 * @apiSuccess {String} books.title Title of the book.
 * @apiSuccess {Number} books.authorId Author's unique ID.
 * @apiSuccess {Number} books.categoryId Category's unique ID.
 * @apiSuccess {Number} books.publicationYear Year of publication.
 * @apiSuccess {String} books.description Description of the book.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "books": [
 *         {
 *           "id": 1,
 *           "title": "Book Title",
 *           "authorId": 1,
 *           "categoryId": 1,
 *           "publicationYear": 2020,
 *           "description": "A brief description of the book."
 *         }
 *       ],
 *       "page": 1,
 *       "limit": 10,
 *       "totalPages": 5
 *     }
 *
 * @apiError BadRequest The query parameters are invalid.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Invalid query parameters"
 *     }
 */

/**
 * @api {get} /books/:id Retrieve a single book
 * @apiName GetBook
 * @apiGroup Book
 *
 * @apiParam {Number} id Book's unique ID.
 *
 * @apiSuccess {Number} id Book ID.
 * @apiSuccess {String} title Title of the book.
 * @apiSuccess {Number} authorId Author's unique ID.
 * @apiSuccess {Number} categoryId Category's unique ID.
 * @apiSuccess {Number} publicationYear Year of publication.
 * @apiSuccess {String} description Description of the book.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "title": "Book Title",
 *       "authorId": 1,
 *       "categoryId": 1,
 *       "publicationYear": 2020,
 *       "description": "A brief description of the book."
 *     }
 *
 * @apiError NotFound The book with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Book not found"
 *     }
 */

/**
 * @api {put} /books/:id Update a book
 * @apiName UpdateBook
 * @apiGroup Book
 *
 * @apiParam {Number} id Book's unique ID.
 *
 * @apiBody {String} [title] Title of the book.
 * @apiBody {Number} [authorId] Author's unique ID.
 * @apiBody {Number} [categoryId] Category's unique ID.
 * @apiBody {Number} [publicationYear] Year of publication.
 * @apiBody {String} [description] Description of the book.
 *
 * @apiSuccess {Number} id Book ID.
 * @apiSuccess {String} title Title of the book.
 * @apiSuccess {Number} authorId Author's unique ID.
 * @apiSuccess {Number} categoryId Category's unique ID.
 * @apiSuccess {Number} publicationYear Year of publication.
 * @apiSuccess {String} description Description of the book.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "title": "Updated Book Title",
 *       "authorId": 1,
 *       "categoryId": 1,
 *       "publicationYear": 2020,
 *       "description": "Updated description of the book."
 *     }
 *
 * @apiError NotFound The book with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Book not found"
 *     }
 */

/**
 * @api {delete} /books/:id Delete a book
 * @apiName DeleteBook
 * @apiGroup Book
 *
 * @apiParam {Number} id Book's unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 No Content
 *
 * @apiError NotFound The book with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Book not found"
 *     }
 */

```

### Author Endpoints

```javascript

/**
 * @api {post} /authors Create a new author
 * @apiName CreateAuthor
 * @apiGroup Author
 *
 * @apiBody {String} name Name of the author.
 * @apiBody {String} bio Biography of the author.
 *
 * @apiSuccess {Number} id The new author's ID.
 * @apiSuccess {String} name Name of the author.
 * @apiSuccess {String} bio Biography of the author.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "name": "Author Name",
 *       "bio": "Biography of the author."
 *     }
 *
 * @apiError BadRequest The request body is invalid.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Invalid request body"
 *     }
 */

/**
 * @api {get} /authors Retrieve a list of authors
 * @apiName GetAuthors
 * @apiGroup Author
 *
 * @apiParam {Number} [page=1] Page number for pagination.
 * @apiParam {Number} [limit=10] Number of items per page.
 * @apiParam {String} [name] Filter authors by name.
 *
 * @apiSuccess {Object[]} authors List of authors.
 * @apiSuccess {Number} authors.id Author ID.
 * @apiSuccess {String} authors.name Name of the author.
 * @apiSuccess {String} authors.bio Biography of the author.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "authors": [
 *         {
 *           "id": 1,
 *           "name": "Author Name",
 *           "bio": "Biography of the author."
 *         }
 *       ],
 *       "page": 1,
 *       "limit": 10,
 *       "totalPages": 2
 *     }
 *
 * @apiError BadRequest The query parameters are invalid.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Invalid query parameters"
 *     }
 */

/**
 * @api {get} /authors/:id Retrieve a single author
 * @apiName GetAuthor
 * @apiGroup Author
 *
 * @apiParam {Number} id Author's unique ID.
 *
 * @apiSuccess {Number} id Author ID.
 * @apiSuccess {String} name Name of the author.
 * @apiSuccess {String} bio Biography of the author.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Author Name",
 *       "bio": "Biography of the author."
 *     }
 *
 * @apiError NotFound The author with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Author not found"
 *     }
 */

/**
 * @api {put} /authors/:id Update an author
 * @apiName UpdateAuthor
 * @apiGroup Author
 *
 * @apiParam {Number} id Author's unique ID.
 *
 * @apiBody {String} [name] Name of the author.
 * @apiBody {String} [bio] Biography of the author.
 *
 * @apiSuccess {Number} id Author ID.
 * @apiSuccess {String} name Name of the author.
 * @apiSuccess {String} bio Biography of the author.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Updated Author Name",
 *       "bio": "Updated biography of the author."
 *     }
 *
 * @apiError NotFound The author with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Author not found"
 *     }
 */

/**
 * @api {delete} /authors/:id Delete an author
 * @apiName DeleteAuthor
 * @apiGroup Author
 *
 * @apiParam {Number} id Author's unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 No Content
 *
 * @apiError NotFound The author with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Author not found"
 *     }
 */

```

### Category Endpoints

```javascript

/**
 * @api {post} /categories Create a new category
 * @apiName CreateCategory
 * @apiGroup Category
 *
 * @apiBody {String} name Name of the category.
 *
 * @apiSuccess {Number} id The new category's ID.
 * @apiSuccess {String} name Name of the category.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 Created
 *     {
 *       "id": 1,
 *       "name": "Category Name"
 *     }
 *
 * @apiError BadRequest The request body is invalid.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Invalid request body"
 *     }
 */

/**
 * @api {get} /categories Retrieve a list of categories
 * @apiName GetCategories
 * @apiGroup Category
 *
 * @apiSuccess {Object[]} categories List of categories.
 * @apiSuccess {Number} categories.id Category ID.
 * @apiSuccess {String} categories.name Name of the category.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "categories": [
 *         {
 *           "id": 1,
 *           "name": "Category Name"
 *         }
 *       ]
 *     }
 *
 * @apiError BadRequest The request parameters are invalid.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "error": "Invalid request parameters"
 *     }
 */

/**
 * @api {get} /categories/:id Retrieve a single category
 * @apiName GetCategory
 * @apiGroup Category
 *
 * @apiParam {Number} id Category's unique ID.
 *
 * @apiSuccess {Number} id Category ID.
 * @apiSuccess {String} name Name of the category.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Category Name"
 *     }
 *
 * @apiError NotFound The category with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Category not found"
 *     }
 */

/**
 * @api {put} /categories/:id Update a category
 * @apiName UpdateCategory
 * @apiGroup Category
 *
 * @apiParam {Number} id Category's unique ID.
 *
 * @apiBody {String} [name] Name of the category.
 *
 * @apiSuccess {Number} id Category ID.
 * @apiSuccess {String} name Name of the category.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 1,
 *       "name": "Updated Category Name"
 *     }
 *
 * @apiError NotFound The category with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Category not found"
 *     }
 */

/**
 * @api {delete} /categories/:id Delete a category
 * @apiName DeleteCategory
 * @apiGroup Category
 *
 * @apiParam {Number} id Category's unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 No Content
 *
 * @apiError NotFound The category with the given ID was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Category not found"
 *     }
 */

```

----------

## Additional Considerations

### Authentication

- **Token-based Authentication:** Each request to the API should include a valid token in the `Authorization` header.
- **Example:** `Authorization: Bearer <token>`
  
### Errors

- `400 Bad Request`: Invalid request parameters or body.
- `401 Unauthorized`: Authentication failure or missing token.
- `403 Forbidden`: User does not have permission to perform the operation.
- `404 Not Found`: Resource not found.
- `500 Internal Server Error`: Server encountered an error.

### Pagination

#### Parameters

- `page` (default: 1): Page number.
- `limit` (default: 10): Number of items per page.

#### Response

- `totalItems`: Total number of items.
- `totalPages`: Total number of pages.
- `currentPage`: Current page number.
- `items`: List of items on the current page.

#### Caching

- GET requests should be cached to improve performance.
- Use HTTP headers like `Cache-Control` to define caching policies.
- Example: `Cache-Control: max-age=3600` (cache for 1 hour).
